const axios = require('axios');

const jsonItems = require('./items').items;
const jsonOrders = require('./orders').orders;
const prices = {
  1: 1,
  5: 2,
  10: 3,
  20: 5,
  Infinity: 10,
};

async function generateParcel(order, paletteNumber) {
  const random = await axios.post('https://helloacm.com/api/random/?n=15');
  const result = {
    order_id: order.id,
    items: [],
    weight: 0,
    tracking_id: random.data,
    palette_number: paletteNumber,
  };

  for (const item of order.items) {
    const itemObj = jsonItems.find((jsonItem) => jsonItem.id === item.item_id);

    for (let i = item.quantity; i > 0; i--) {
      result.items.push(itemObj);
      result.weight += parseFloat(itemObj.weight).toPrecision(2);
    }
  }

  if (result.weight > 30) {
    result.error = 'Parcel weight exceed 30kg';
    return result;
  }

  return result;
}

function getRevenue(parcels) {
  let revenue = 0;

  for (const parcel of parcels) {
    for (const maxW of Object.keys(prices)) {
      if (parcel.weight < maxW) {
        revenue += prices[maxW];
        break;
      }
    }
  }

  return revenue;
}

async function main() {
  let palette_number = 0;
  const promises = [];

  for (const order of jsonOrders) {
    promises.push(new Promise(async (resolve) => {
      try {
        const parcels = await generateParcel(order, ++palette_number);
        resolve(parcels);
      } catch (e) {
        resolve(e);
      }
    }));
  }
  const parcels = (await Promise.all(promises)).filter((parcel) => parcel !== undefined);
  const revenue = getRevenue(parcels);

  parcels.map((parcel) => {
    console.log('Palette n°' + parcel.palette_number + ' : ');

    if (parcel.error) {
      console.log('Error : ' + parcel.error);
      return;
    }

    console.log('	Parcels : ' + JSON.stringify(parcels));
    console.log();
  });
  console.log('Revenue : ' + revenue + '€');
}

main();


